import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { HomeComponent } from './home/home.component';
import {SharedModule} from '../shared/shared.module';
import { CarouselComponent } from './carousel/carousel.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { WhoWeAreComponent } from './who-we-are/who-we-are.component';
import { FeaturedProductsComponent } from './featured-products/featured-products.component';
import { ShopByPartsComponent } from './shop-by-parts/shop-by-parts.component';
import { MaintenanceProductsComponent } from './maintenance-products/maintenance-products.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { SocialMediaComponent } from './social-media/social-media.component';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    NgbModule
  ],
  declarations: [HomeComponent, CarouselComponent, WhoWeAreComponent,
                FeaturedProductsComponent, ShopByPartsComponent,
                MaintenanceProductsComponent, NewsletterComponent, SocialMediaComponent]
})
export class MainModule { }
