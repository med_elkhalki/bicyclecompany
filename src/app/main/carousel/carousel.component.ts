import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  public images = ['../../../assets/images/carousel-1.jpg', '../../../assets/images/carousel-2.jpeg', '../../../assets/images/carousel-3.jpg'];
  public showNavigationArrows = true;
  public showNavigationIndicators = false;

  constructor() { }

  ngOnInit() {
  }

}
