import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ScrollToDirective} from './directives/scroll-to.directive';


@NgModule({
  imports: [
    CommonModule,
    NgbModule,
  ],
  declarations: [HeaderComponent, FooterComponent, ScrollToDirective],
  exports: [HeaderComponent, FooterComponent, ScrollToDirective],
  providers: []
})
export class SharedModule { }
